#pragma once

#include <string>
#include <vector>
#include <map>
#include <memory>


class EnJaParser {
private:
    std::string buf;
    std::vector<std::string> subenja;
    void reset();
    std::unique_ptr<std::map<std::string, std::string>> ENJA;

public:
    EnJaParser(std::vector<std::pair<std::string, std::string>>);
    std::string add_character(char ch);
    ~EnJaParser();
};
