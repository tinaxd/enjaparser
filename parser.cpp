#include "parser.hpp"
#include <regex>
#include <algorithm>
#include <iostream>

bool vector_find(std::vector<std::string> vec, std::string el)
{
    auto itr = std::find(vec.begin(), vec.end(), el);
    size_t index = std::distance(vec.begin(), itr);
    if (index != vec.size()) return true;
    else return false;
}

EnJaParser::EnJaParser(std::vector<std::pair<std::string, std::string>> pairs)
{
    this->buf = "";
    ENJA.reset(new std::map<std::string, std::string>);
    for (auto it = pairs.begin(); it != pairs.end(); it++)
    {
        ENJA->insert(*it);
    }
}

EnJaParser::~EnJaParser()
{

}

void EnJaParser::reset()
{
    this->subenja.clear();
    this->buf = "";
}

std::string EnJaParser::add_character(char ch)
{
    /*
    std::cout << ">> Current buf: " << buf << std::endl;
    std::cout << ">> Current subenjas: " << std::endl;
    for (auto it = subenja.begin(); it != subenja.end(); it++)
    {
        std::cout << "  >> " << *it << std::endl;
    }
    std::cout << ">> New character " << std::string(1, ch) << std::endl; */
    switch (this->buf.size())
    {
        case 0: {
            if (ch == 'a' || ch == 'i' || ch == 'u' || ch == 'e' || ch == 'o')
            {
                reset();
                return ENJA->at(std::string(1, ch));
            }
            else
            {
                bool a = false;
                for (auto it = ENJA->begin();
                     it != ENJA->end(); it++)
                     {
                         if (it->first.at(0) == ch)
                         {
                             a = true;
                             break;
                         }
                     }
                if (a)
                {
                    buf.push_back(ch);
                    subenja.clear();
                    for (auto it = ENJA->begin();
                         it != ENJA->end(); it++)
                         {
                             if (it->first.at(0) == ch)
                             {
                                 subenja.push_back(it->first);
                             }
                         }
                    return "";
                }
                else
                {
                    std::regex re(R"([a-z])");
                    if (!std::regex_match(std::string(1, ch), re))
                    {
                        return std::string(1, ch);
                    }
                    else
                    {
                        this->reset();
                        return "E";
                    }
                }
            }
        }
        case 1: {
            auto ok = vector_find(subenja, this->buf + ch);
            if (ok)
            {
                auto ans = this->buf + ch;
                this->reset();
                return ENJA->at(ans);
            }
            else
            {
                bool a = false;
                for (std::vector<std::string>::iterator it = subenja.begin();
                     it != subenja.end(); it++)
                     {
                         if (it->at(1) == ch)
                         {
                             a = true;
                             break;
                         }
                     }
                if (a)
                {
                    buf.push_back(ch);
                    std::vector<std::string> oldsub(subenja.size());
                    std::copy(subenja.begin(), subenja.end(), oldsub.begin());
                    subenja.clear();
                    for (auto it = oldsub.begin(); it != oldsub.end(); it++)
                         {
                             if (it->at(1) == ch)
                             {
                                 subenja.push_back(*it);
                             }
                         }
                    return "";
                }
                else
                {
                    bool b = false;
                    for (auto it = ENJA->begin(); it != ENJA->end(); it++)
                    {
                        if (it->first.at(0) == ch)
                        {
                            b = true;
                            break;
                        }
                    }
                    if (buf.at(0) == 'n' && b)
                    {
                        buf = ch;
                        subenja.clear();
                        for (auto it = ENJA->begin(); it != ENJA->end(); it++)
                        {
                            if (it->first.at(0) == ch)
                            {
                                subenja.push_back(it->first);
                            }
                        }
                        return "ん";
                    }
                    else
                    {
                        this->reset();
                        return "E";
                    }
                }
            }
            break;
        }
        case 2:

        case 3: {
            int casen = this->buf.size();
            auto ok2 = vector_find(subenja, this->buf + ch);
            if (ok2)
            {
                auto ans = this->buf + ch;
                this->reset();
                return ENJA->at(ans);
            }
            else
            {
                bool a = false;
                for (auto it = subenja.begin(); it != subenja.end(); it++)
                {
                    if (it->at(casen) == ch)
                    {
                        a = true;
                        break;
                    }
                }
                if (a)
                {
                    this->buf.push_back(ch);
                    std::vector<std::string> oldsub(subenja.size());
                    std::copy(subenja.begin(), subenja.end(), oldsub.begin());
                    subenja.clear();
                    for (auto it = oldsub.begin(); it != oldsub.end(); it++)
                    {
                        if (it->at(casen) == ch)
                        {
                            subenja.push_back(*it);
                        }
                    }
                    return "";
                }
                else
                {
                    this->reset();
                    return "E";
                }
            }
            break;
        }
        case 4: {
            auto ok3 = vector_find(subenja, this->buf + ch);
            if (ok3)
            {
                auto ans = this->buf + ch;
                this->reset();
                return ENJA->at(ans);
            }
            else
            {
                this->reset();
                return "E";
            }
            break;
        }
    }
    return "E";
}
