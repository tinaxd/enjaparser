#include <string>
#include "parser.hpp"
#include <iostream>
#include <fstream>
#include <map>

using namespace std;

int main()
{
    char str1[10];
    char str2[10];
    ifstream ifs("./parseData.txt");
    vector<pair<string, string>> data;
    while (ifs.getline(str1, 10)) {
        ifs.getline(str2, 10);
        data.push_back(make_pair(string(str1), string(str2)));
    }
    EnJaParser* parser = new EnJaParser(data);
    cout << parser->add_character('a') << endl;
    cout << parser->add_character('n') << endl;
    cout << parser->add_character('g') << endl;
    cout << parser->add_character('a') << endl;
    cout << parser->add_character('y') << endl;
    cout << parser->add_character('a') << endl;
    cout << parser->add_character('l') << endl;
    cout << parser->add_character('t') << endl;
    cout << parser->add_character('u') << endl;
    cout << parser->add_character('n') << endl;
    cout << parser->add_character('e') << endl;
    delete parser;
    return 0;
}
