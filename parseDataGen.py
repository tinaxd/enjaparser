import json

with open('./fromjs.json', mode='r') as f:
    d = json.loads(f.read())
    print('finish reading')

with open('./parseData.txt', mode='w') as w:
    for k, v in d.items():
        w.write(k + "\n")
        w.write(v + "\n")

print('wrote ' + str(len(d)) + ' lines')
